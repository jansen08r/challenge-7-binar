import { Routes, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css' // 2 poin ini harus diimport buat boostrap
import 'bootstrap/dist/js/bootstrap.bundle.js' // kalo ini js nya
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import HomeUtama from "./pages/HomeUtama";
import './pages/style.css'
import Footer from './components/Footer';


function App() {

  return (
    <div className="App">
      <Navbar />
      <Routes>
          <Route path='/' element={<HomeUtama />} />
          <Route path='/cars' element={<HomePage />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
